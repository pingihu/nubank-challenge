(defproject nubank-server "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [org.clojure/data.priority-map "0.0.7"]
                 [org.clojure/core.match "0.3.0-alpha4"]
                 [org.clojure/math.combinatorics "0.1.1"]
                 [org.clojure/tools.cli "0.3.3"]
                 [aysylu/loom "0.5.4"] ; used for tests
                 [compojure "1.4.0"]
                 [hiccup "1.0.5"]
		 		 [ring/ring-core "1.4.0"]
		 		 [ring/ring-jetty-adapter "1.4.0"]
		 		 [ring/ring-defaults "0.1.5"]]
  :main nubank-server.core)
