(ns nubank-server.graph-io-test
 (:use [clojure.math.combinatorics :only (combinations)])
 (:require [clojure.test :refer :all]
           [nubank-server.graph-io :refer :all]
           [nubank-server.graph-paths :refer :all]
           [loom.graph :as loom]
           [loom.alg :as loom-alg]
           [loom.gen :as loom-gen]))

(def wg (loom-gen/gen-rand-p (loom/weighted-graph [:1 :2 1]) 100 0.5 :min-weight 1 :max-weight 2))
(def real-wg
  (add-edges (:edges wg)))
(def file-wg
	(reduce loom/add-edges (loom/weighted-graph)
		(for [e (clojure.string/split (slurp "resources/edges.txt") #"\n")]
			(let [edge (clojure.string/split e #" ")]
				[(keyword (first edge)) (keyword (second edge)) 1]))))
(def real-file-wg (make-graph "edges.txt"))

(deftest is-valid-edge-test
  (are [edge res] (= (not (nil? (apply is-valid-edge edge))) res)
    [""] false
    ["n      	n"] false
    [" "] false
    ["3 4"] true))

(deftest make-keywords-test 
  (are [ks kwds] (= (apply make-keywords ks) kwds)
    [""] nil
    [" "] nil
    ["    "] nil
    ["ab"] nil
    ["   v1 v2"] nil
    ["45 54 54"] nil
    ["v1 v2"] [:v1 :v2]))

(deftest add-edge-test
  (let [example-graph {:1 {:2 1, :4 1},
  	                   :2 {:1 1},
  	                   :4 {:1 1}}]
    (are [args res] (= (apply add-edge args) res)
      [{} [:1 :2]] {:1 {:2 1},
                    :2 {:1 1}}
      [example-graph [:4 :2]] {:1 {:2 1, :4 1},
  	                           :2 {:1 1 :4 1},
  	                           :4 {:1 1 :2 1}}
  	  [example-graph [:3 :5]] {:1 {:2 1, :4 1},
  	                           :2 {:1 1},
  	                           :4 {:1 1},
  	                           :3 {:5 1},
  	                           :5 {:3 1}}
  	  [example-graph [:4 :2] 5] {:1 {:2 1, :4 1},
  	                           :2 {:1 1 :4 5},
  	                           :4 {:1 1 :2 5}}
  	  [example-graph [:4 :1] 5] {:1 {:2 1, :4 5},
  	                             :2 {:1 1},
  	                             :4 {:1 5}}
  	  [example-graph [:4 :3] 2] {:1 {:2 1, :4 1},
  	                             :2 {:1 1},
  	                             :4 {:1 1, :3 2}
  	                             :3 {:4 2}})))

(deftest add-edges-test
  (let [real-graph real-file-wg
  	    expected-graph file-wg]
  	(doseq [e (loom/edges expected-graph)]
  	  (let [[v1 v2] e]
  	    (= (get-in real-graph [v1] [v2])
  	       (loom/weight expected-graph v1 v2))))))

(deftest all-shortest-paths-file-test
	(def start2 (System/currentTimeMillis))
  (let [real-paths (calculate-all-distances real-file-wg)
  	    all-nodes (:nodeset file-wg)]
  	(doseq [[v1 v2] (combinations all-nodes 2)]
  	  (let [expected-dist  (second (loom-alg/dijkstra-path-dist file-wg v1 v2))]
  	  (= (get-in real-paths [v1] [v2]) expected-dist)))))

(deftest all-shortest-paths-rand-test
  (let [real-paths (calculate-all-distances real-wg)
  	    all-nodes (:nodeset wg)]
  	(doseq [[v1 v2] (combinations all-nodes 2)]
  	  (let [expected-dist  (second (loom-alg/dijkstra-path-dist wg v1 v2))]
  	  (= (get-in real-paths [v1] [v2]) expected-dist)))))