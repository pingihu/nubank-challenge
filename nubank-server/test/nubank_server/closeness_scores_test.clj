(ns nubank-server.closeness-scores-test
  (:require [clojure.test :refer :all]
  	        [nubank-server.closeness-scores :refer :all]
  	        [loom.gen :as loom-gen]
  	        [loom.graph :as loom]))

(def sparse-wg {:3 {} :4 {} :5 {}})
(def separate-wg {:4 {:5 1} :5 {:4 1} :3 {}})
(def wg
  (loom-gen/gen-rand-p (loom/weighted-graph [:1 :2 1]) 100 0.5 :min-weight 1 :max-weight 2))

(deftest calculate-closeness-test
  (are [g res] (= (apply calculate-closeness g) res)
  	[sparse-wg] {:3 [Integer/MIN_VALUE 1]
  		         :4 [Integer/MIN_VALUE 1]
  		         :5 [Integer/MIN_VALUE 1]}
    [separate-wg] {:4 [1 1]
    	           :5 [1 1]
                   :3 [Integer/MIN_VALUE 1]}))