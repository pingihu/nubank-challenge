(ns nubank-server.fraud-detection-test
  (:require [clojure.test :refer :all]
  	        [clojure.core.match :refer [match]]
  	        [nubank-server.closeness-scores :refer :all]
  	        [nubank-server.fraud-detection :refer :all]
  	        [loom.gen :as loom-gen]
  	        [loom.graph :as loom]))

(def sparse-wg {:3 {} :4 {} :5 {}})
(def separate-wg {:4 {:5 1} :5 {:4 1} :3 {}})

(deftest mark-fraud-test
  (is (= (mark-fraud sparse-wg (calculate-closeness sparse-wg) :5)
    {:3 [Integer/MIN_VALUE 1]
     :4 [Integer/MIN_VALUE 1]
     :5 [Integer/MIN_VALUE 0]}))
  (is (= (mark-fraud separate-wg (calculate-closeness separate-wg) :5)
    {:4 [1 0.5]
     :5 [1 0]
     :3 [Integer/MIN_VALUE 1]})))