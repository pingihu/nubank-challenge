(ns nubank-server.graph-paths
  (:use [clojure.math.combinatorics :only (combinations)])
  (:require [nubank-server.graph-io :as graph-io]))

(defn calculate-distance
  "Calculates shortest distance from v2 to v3 through v1."
  [graph [v1 v2 v3]]
  (let [poss-new-dist (+ (or (get-in graph [v2 v1]) Integer/MAX_VALUE)
	    				 (or (get-in graph [v1 v3]) Integer/MAX_VALUE))]
    (if (= v2 v3)
      (graph-io/add-edge graph [v2 v3] 0)
      (if (> (or (get-in graph [v2 v3]) Integer/MAX_VALUE) poss-new-dist)
      	(graph-io/add-edge graph [v2 v3] poss-new-dist)
      	graph))))

(defn distance-through-k 
  "Calculates shortest distances from vertex s to e through each other vertex k."
  [graph [s e]]
  (reduce calculate-distance graph (for [v (keys graph)] [v s e])))

(defn calculate-all-distances
  "Calculates all initial shortest distances in the graph."
  [graph]
  (let [edges (combinations (keys graph) 2)]
    (reduce distance-through-k graph edges)))