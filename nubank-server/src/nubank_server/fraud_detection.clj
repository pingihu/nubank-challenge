(ns nubank-server.fraud-detection)

(defn fraud-constant [k] (- 1 (Math/pow 0.5 k)))

(defn update-connected-score
  "Updates the fraud factors of all connected vertices. "
  [scores [vertex k]]
  (if (zero? k)
    scores ; not connected or fraud vertex which has already been updated.
    (assoc scores vertex [k (fraud-constant k)])))

(defn mark-fraud
  "Marks one vertex as fraud and updates its fraud factor and its neighbors'."
  [graph scores fraud]
  (let [[fraud-closeness _] (fraud scores)]
    (reduce update-connected-score (assoc scores fraud [fraud-closeness 0]) (fraud graph))))