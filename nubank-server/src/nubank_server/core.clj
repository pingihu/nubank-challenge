(ns nubank-server.core
  (:use [hiccup.element :only (ordered-list)]
        [hiccup.page :only (html5 include-css include-js)])
  (:require [nubank-server.closeness-scores :as score-tool]
            [nubank-server.fraud-detection :as fraud-tool]
            [nubank-server.graph-io :as graph-io]
            [nubank-server.graph-paths :as path-tool]
            [clojure.tools.cli :refer [parse-opts]]
            [compojure.core :refer [defroutes GET POST]]
            [ring.adapter.jetty :as ring]
  		 	    [ring.middleware.defaults :refer :all]
  		 	    [ring.middleware.params :refer [wrap-params]]
  			    [ring.util.response :as ring-response]))

(defn- initialize-graph [f]
  (let [graph (graph-io/make-graph f)]
    (path-tool/calculate-all-distances graph)))

(defn- get-scores 
  ([g] (score-tool/calculate-closeness g))
  ([g s] (score-tool/calculate-closeness g s)))

(def current-dgraph (atom nil)) ;(initialize-graph)))
(def current-scores (atom nil)) ;(get-scores @current-dgraph)))

(defn update-graph
  "Updates graph with a new edge."
  [g e]
  (path-tool/calculate-all-distances (graph-io/add-edge g e 1))); e))

(def memo-update-graph (memoize update-graph))

(defn register-edge
  "Endpoint to register an edge."
  [v1 v2]
  (let [edge-string (str v1 " " v2)]
    (if-let [edge (graph-io/make-keywords edge-string)]
      (do (swap! current-dgraph memo-update-graph edge)
          (reset! current-scores (get-scores @current-dgraph @current-scores))
          (str "Successfuly registered " edge-string "\n"))
      (str "Could not register edge " edge-string "\n"))))

(defn report-fraud 
  "Endpoint to report a fraudulent vertex."
  [vertex]
  (let [v-key (keyword vertex)]
    (if (get @current-scores v-key)
      (do (reset! current-scores (fraud-tool/mark-fraud @current-dgraph @current-scores v-key))
          (str "Successfully marked " vertex " as fraudulent.\n"))
      (str vertex " does not exist in our graph. \n"))))

(defn render-ranking
  "Endpoint to render the current ranking of vertices."
  [req]
	(html5 (ordered-list (map key (score-tool/rank-closeness @current-scores)))))

(defroutes routes
	(GET "/rank" [] render-ranking)
	(POST "/register" [v1 v2] (register-edge v1 v2))
  (POST "/fraud" [v] (report-fraud v)))

(def application (wrap-params routes))

(defn start-app [port file]
  (reset! current-dgraph (initialize-graph file))
  (reset! current-scores (get-scores @current-dgraph))
  (ring/run-jetty application {:port port
                               :join? false}))

(def cli-args
  [["-p" "--port <port>" "Port number to run the server on."
    :default 3000
    :parse-fn #(Integer/parseInt %)
    :validate [#(< 0 % 0x10000) "Must be a number between 0 and 65536"]]
   ["-f" "--file <graph-file>" "File to build initial graph."
    :default "edges.txt"]
   ["-h" "--help"]])

(defn -main [& args]
  (let [{opts :options} (parse-opts args cli-args)]
    (start-app (:port opts) (:file opts))))
   

    ; (ring/run-jetty application {:port (:port opts)
    ;                              :join? false})))