(ns nubank-server.graph-io
  (:require [clojure.java.io :as io]
	          [clojure.string :as str]))

;; Represents the undirected graph as a two-level hash-map.
;; Each key is a vertex in the graph.
;; Each value is a map of vertices connected to that key
;; to their distance from that key.
;; {v1 : {v2 : 1
;;		  v3 : 2}
;;  v2 : {v1 : 1
;;	      v3 : 1}
;;  v3 : {v2 : 1
;;		  v1 : 2}}
;; represents
;; v1 - v2 - v3

(defn is-valid-edge
  "Edges are represented as a source end string."
  [e]
  (re-matches #"[^\s]+\s[^\s]+" e))

(defn make-keywords
  "Creates a keyword pair representing an edge."
  [kwds] 
  (if (is-valid-edge kwds)
    (map keyword (str/split kwds #" "))
    nil))

(defn add-edge
  "Adds a connection between two vertices in the graph.
   Edges have distance 1."
  [m vs & [n]]
  (reduce #(update-in % [(first %2)] assoc (second %2) (or n 1)) m [vs (reverse vs)]))

(defn add-edges [edges] (reduce add-edge {} edges))

(defn make-graph
  "Creates a new graph from a file. "
  [file]
  (with-open [rdr (io/reader (io/resource file))]
    (add-edges (keep make-keywords (line-seq rdr)))))