(ns nubank-server.closeness-scores)

;; Represents closeness scores as a map
;; from vertex to [closeness, fraud-factor]
;; where the fraud factor changes.

(defn calculate-closeness-score
  "Calculates closeness score by taking the inverse of the distance sum."
  [distances]
  (let [farness (reduce + 0 (vals distances))]
    (if (zero? farness)
      Integer/MIN_VALUE
      (/ 1 farness))))

(defn calculate-closeness
  "Calculates score and fraud factor for each map."
  ([distance-graph]
    (into {} 
      (for [k (keys distance-graph)]
        [k [(calculate-closeness-score (k distance-graph)) 1]])))
  ([distance-graph prev-scores]
    (into {} 
      (for [k (keys distance-graph)]
        [k [(calculate-closeness-score (k distance-graph)) (or (second (k prev-scores)) 1)]]))))

(defn rank-closeness
  "Ranks vertices by their closeness scores times a fraud factor."
  [closeness-scores]
	(sort
      (fn [[k [v c]] [k2 [v2 c2]]] (compare (* v2 c2) (* v c)))
      closeness-scores))